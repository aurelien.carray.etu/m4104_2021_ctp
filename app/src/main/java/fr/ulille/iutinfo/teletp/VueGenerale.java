package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle, poste, DISTANCIEL;

    // TODO Q2.c
    SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        this.DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = this.DISTANCIEL;

        // TODO Q2.c
        this.model = new SuiviViewModel(getActivity().getApplication());

        // TODO Q4
        Spinner spSalle = (Spinner) view.findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adapter1);
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
               update();
            } // to close the onItemSelected
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        Spinner spPoste = (Spinner) view.findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getContext(), R.array.list_postes, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(adapter2);
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                update();
            } // to close the onItemSelected
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
            TextView etLogin = (TextView) view.findViewById(R.id.tvLogin);
            this.model.setUsername(etLogin.getText().toString());
        });

        // TODO Q5.b
        this.update();

        // TODO Q9
    }

    // TODO Q5.a
    void update() {
        Spinner spSalle = (Spinner) this.getView().findViewById(R.id.spSalle);
        Spinner spPoste = (Spinner) this.getView().findViewById(R.id.spPoste);

        if (spSalle.getSelectedItemPosition() == 0) {
            spPoste.setVisibility(View.GONE);
            spPoste.setEnabled(false);
            this.model.setLocalisation(getActivity().getResources().getStringArray(R.array.list_salles)[0]);
            return;
        }
        else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
        }

        String [] tmp = getActivity().getResources().getStringArray(R.array.list_salles);
        this.DISTANCIEL = tmp[spSalle.getSelectedItemPosition()];
        System.out.println(this.DISTANCIEL);

        String loc = spSalle.getSelectedItem().toString() + " : " + spPoste.getSelectedItem().toString();
        this.model.setLocalisation(loc);

    }


    // TODO Q9
}